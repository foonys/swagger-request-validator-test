package com.gk_software.test.swagger_request_validator_test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.FileUtils;


public class Request implements com.atlassian.oai.validator.model.Request {

  private static Map<String, Collection<String>> HEADERS = new HashMap<>();
  static {
    List<String> contentType = new ArrayList<>();
    contentType.add("application/json");
    HEADERS.put("Content-Type", contentType);
    HEADERS.put("Accept", contentType);
    HEADERS.put("Cookie", new ArrayList<>());
  }

  private String requestFilePath;
  private String requestUrl;

  public Request(String requestFilePath, String requestUrl) {
    this.requestFilePath = requestFilePath;
    this.requestUrl = requestUrl;
  }

  @Override
  public Collection<String> getQueryParameters() {
    return new ArrayList<>();
  }

  @Override
  public Collection<String> getQueryParameterValues(String name) {
    return new ArrayList<>();
  }

  @Override
  public String getPath() {
    return requestUrl;
  }

  @Override
  public Method getMethod() {
    return Method.POST;
  }

  @Override
  public Map<String, Collection<String>> getHeaders() {
    return HEADERS;
  }

  @Override
  public Collection<String> getHeaderValues(String name) {
    return getHeaders().get(name);
  }

  @Override
  public Optional<String> getBody() {
    String body = null;
    try {
      body = FileUtils.readFileToString(new File(requestFilePath), Charset.forName("utf-8"));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return Optional.of(body);
  }
}
