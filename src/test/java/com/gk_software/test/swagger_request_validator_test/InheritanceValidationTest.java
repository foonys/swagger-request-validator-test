package com.gk_software.test.swagger_request_validator_test;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;

import com.atlassian.oai.validator.OpenApiInteractionValidator;
import com.atlassian.oai.validator.report.ValidationReport;


public class InheritanceValidationTest {

  private static final String OA3_URL = "/oa3/api-with-inheritance.yaml";

  private static OpenApiInteractionValidator VALIDATOR = OpenApiInteractionValidator.createFor(OA3_URL).build();

  @Test
  public void testValidation() throws Exception {
    ValidationReport report = VALIDATOR.validateRequest(new Request("src/test/resources/inheritance-request.json", "/services/rest/api-with-inheritance/v1/send-object-with-inheritance"));
    System.out.println(report.getMessages());
    assertFalse(report.hasErrors());

//    report = VALIDATOR.validateRequest(new Request("src/test/resources/inheritance-request.json", "/services/rest/api-with-inheritance/v1/send-object-with-inheritance"));
//    System.out.println(report.getMessages());
//    assertFalse(report.hasErrors());
  }

}
